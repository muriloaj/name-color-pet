#!/usr/bin/env python3

import boto3
import json
import os
from datetime import date, timedelta, datetime
import logging
import time
import datetime
import string
import pymysql

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s : [%(levelname)s] (%(threadName)-10s) -  %(name)s - %(message)s',
                    )

logging.info("AWS_ACCESS_KEY_ID={}".format(len(str(os.environ['AWS_ACCESS_KEY_ID']))))
logging.info("AWS_SECRET_ACCESS_KEY={}".format(
    len(str(os.environ['AWS_SECRET_ACCESS_KEY']))))
logging.info("AWS_SQS_QUEUE={}".format(str(os.environ['AWS_SQS_QUEUE'])))
logging.info('AWS_DEFAULT_REGION={}'.format(str(os.environ['AWS_DEFAULT_REGION'])))
logging.info("AWS_SQS_QUEUE_PULLSIZE={}".format(
    str(os.environ['AWS_SQS_QUEUE_PULLSIZE'])))

#Mysql connection
logging.info("AWS_RDS_HOST={}".format(str(os.environ['AWS_RDS_HOST'])))
logging.info("AWS_RDS_DATABASE={}".format(str(os.environ['AWS_RDS_DATABASE'])))
logging.info("AWS_RDS_USERNAME={}".format(str(os.environ['AWS_RDS_USERNAME'])))
logging.info("AWS_RDS_PASSWORD={}".format(len(str(os.environ['AWS_RDS_PASSWORD']))))


#Variables configured using ENVs
#sqs
sqs_queue_name=str(os.environ['AWS_SQS_QUEUE'])
pull_sqs_size=int(os.environ['AWS_SQS_QUEUE_PULLSIZE'])
idle_cicle_allowed=int(os.environ['IDLE_CICLE_ENABLED'])

## Mysql
mysql_host=str(os.environ['AWS_RDS_HOST'])
mysql_username=str(os.environ['AWS_RDS_USERNAME'])
mysql_password=str(os.environ['AWS_RDS_PASSWORD'])
mysql_database=str(os.environ['AWS_RDS_DATABASE'])




# Create SQS client
# Get the service resource
sqs=boto3.resource('sqs')

queue=sqs.get_queue_by_name(QueueName=sqs_queue_name)


# reference: https://simple.wikipedia.org/wiki/List_of_colors : Chart of selected web colors
list_of_colors=["white", "silver", "grey", "black", "navy", "blue", "cerulean",
                  "sky blue", "turquoise", "blue-green", "azure", "teal", "cyan",
                  "green", "lime", "chartreuse", "olive", "yellow", "gold", "amber",
                  "orange", "brown", "orange-red", "red", "maroon", "rose",
                  "red-violet", "pink", "magenta", "purple", "blue-violet", "indigo",
                  "violet", "peach", "apricot", "ochre", "plum"]

list_of_pets=["cats", "dogs"]

######################
# Code here under
######################

def save_on_mysql(message):
    try:

        logging.info("Processing for MySQL" + json.dumps(message.body) )
        inputt= json.loads(str.replace(message.body,"'","\"") )
        name = inputt["name"],
        pet = inputt["pet"],
        color = inputt["color"]
        

        connection = pymysql.connect(host=mysql_host, 
                                     user=mysql_username, 
                                     password=mysql_password,
                                     db=mysql_database, 
                                     cursorclass=pymysql.cursors.DictCursor)

        with connection.cursor() as cursor:
            #Trust me: 'delete and insert' is faster than 'select and update' for mysql
            deletesql = "DELETE FROM tbl_name_color_pet where name = %s "
            cursor.execute(deletesql, (name))
            # Create a new record
            insertsql = "INSERT INTO `tbl_name_color_pet`(`name`, `pet`, `color`) VALUES (%s,%s,%s);"
            cursor.execute(insertsql, (name, pet, color))
        connection.commit()
        connection.close()

        logging.info(message.delete())

        logging.info("GetItem succeeded: {}-{}={},".format(name, pet, color))
    except Exception as e:
        logging.error(str(e))

    return

def save_on_database(message):
    save_on_mysql(message)

while(True):

    messages=queue.receive_messages(
        MaxNumberOfMessages=pull_sqs_size,
        VisibilityTimeout=30,
        WaitTimeSeconds=5
        )

    for message in messages:
        try:
            save_on_database( message )
        except Exception as e:
            logging.error("Error in execution: {} ".format(str(e)))




######################
# Code over here
######################
