#!/usr/bin/env python3

import boto3
import json
import os
from datetime import date, timedelta, datetime
from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import logging

print('AWS_ACCESS_KEY_ID={}'.format(len(str(os.environ['AWS_ACCESS_KEY_ID']))))
print('AWS_SECRET_ACCESS_KEY={}'.format(len(str(os.environ['AWS_SECRET_ACCESS_KEY']))))
print('AWS_DEFAULT_REGION={}'.format(str(os.environ['AWS_DEFAULT_REGION'])))
print('AWS_SQS_QUEUE={}'.format(str(os.environ['AWS_SQS_QUEUE'])))

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s : [%(levelname)s] (%(threadName)-10s) -  %(name)s - %(message)s',
                    )
# Create SQS client

# Get the service resource
sqs = boto3.resource('sqs')

# Get the queue
sqs_queue_name=str(os.environ['AWS_SQS_QUEUE'])
queue = sqs.get_queue_by_name(QueueName=sqs_queue_name)


app = Flask(__name__)
CORS(app, support_credentials=True)

# reference: https://simple.wikipedia.org/wiki/List_of_colors : Chart of selected web colors
list_of_colors = ['white', 'silver', 'grey', 'black', 'navy', 'blue', 'cerulean',
                  'sky blue', 'turquoise', 'blue-green', 'azure', 'teal', 'cyan',
                  'green', 'lime', 'chartreuse', 'olive', 'yellow', 'gold', 'amber',
                  'orange', 'brown', 'orange-red', 'red', 'maroon', 'rose',
                  'red-violet', 'pink', 'magenta', 'purple', 'blue-violet', 'indigo',
                  'violet', 'peach', 'apricot', 'ochre', 'plum']

list_of_pets = ['cats', 'dogs']

######################
# Code here under
######################

def send_request(request_to_queue):
    logging.info ( 'sending to SQS queue {0} : {1}'.format(sqs_queue_name, request_to_queue))
    response = queue.send_message(MessageBody=str(request_to_queue))
    logging.info ( 'from SQS: {}'.format(json.dumps (response, sort_keys=True)))
    return response


@app.route('/npc-submission', methods=['POST'])
@cross_origin()
def submission_req():
    try:
        data = request.get_json()
        name = data['name'].strip().lower()
        pet = data['pet'].strip().lower()
        color = data['color'].strip().lower()
        logging.info ('Received: '+ json.dumps(data))

        if name == None and len(name) > 0:
            msg = 'name cannot be neither empty nor null.'
            logging.info ( msg )
            return json.dumps({'success': False, 'message': msg }), 400

        if pet not in list_of_pets:
            msg = 'pet -- {} -- is unexpected.'.format(pet)
            return json.dumps({'success': False, 'message': msg, 'expected': list_of_pets}), 400

        if color not in list_of_colors:
            msg = 'color -- {} -- is unexpected.'.format(color)
            return json.dumps({'success': False, 'message': msg, 'expected': list_of_colors}), 400

        request_to_queue = {"name":name,"color":color,"pet":pet}

        logging.info ( ( json.dumps (request_to_queue, sort_keys=True) ) )

        response_from_queue = send_request(request_to_queue)

        return json.dumps({'success': True, 'message': 'sent to server', 'info': response_from_queue}), 201
    except Exception as e:
        logging.error( 'General error in execution: {} '.format(str(e)))
        return json.dumps({'success': False, 'message': 'Check the submission and try again'}), 500


@app.route('/pet-allowed')
@cross_origin()
def pet_list():
    return json.dumps (list_of_pets, sort_keys=True)


@app.route('/color-allowed')
@cross_origin()
def color_list():
    return json.dumps (list_of_colors, sort_keys=True)


######################
# Code over here
######################


if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(host='0.0.0.0', port=6000, debug=False)
