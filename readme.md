Name Pet Color Project
=======================

# Introduction

Sample application to apply some DevOps architectural concepts using queuing messages and Infra As Code (IaC) using Ansible and Kubernetes.
To illustrate it, a survey model will be used to received from final user its _username_, a preferred _pet_ (cats/dogs) and which _color_ fits better with the pet. 

## Accessing the application

API endpoint URL(REST API):


 -  https://npcapi.proaject.co/ - open - Static Web App  
 -  https://npcapi.proaject.co/pet-allowed - return list of pets allowed
 -  https://npcapi.proaject.co/color-allowed - return list of colors allowed
 -  https://npcapi.proaject.co/npc-submission - main endpoint to submit survey.

Alternative frontend Static Web App :

 -  https://s3.amazonaws.com/npc-s3-html/index.html

## Architecture and workflows
The solution is divided in mainly 3 parts, an visual application responsible for the user experience to receive the information provided by user as survey, a front api that receives the messages via REST API and push it into a queue to be processed by backend and a backend application that pulls the messages stored inside the queues and process them properly updating the database inserting or updating the data processed. 

Just for configuration proposes, an ephemeral SSH access was released. It must to be switch off this access when unused.
![Architecture Infrastructure](ncp-doc/ncp-iarc.png)

### Application data workflow
Each part can be allocated in different subnets , or, considering Kubernetes, in different namespaces. For this application they will compound the same namespace to simplify the visualization. 
This application can be scaled horizontally for backend and frontend parts. The part must some attention is the Relational Database because, even managed by AWS, to scale it there is possibility of unavailability on backend side.

Regarding the data workflow, the data follows the sequence bellow:
![Survey data workflow](ncp-doc/ncp-dataflow.png)
- 1st : Rest API (Front Api) receives the data (_name_,_pet_ and _color_);
* 2nd : The Front Api will depurate and push the data inside a queue on Amazon SQS;
* 3rd : The Backend App will pull the data from SQS queue and process properly;
* 4th : The Backend App, after process and store/update inside relational database, Mysql provided by AWS RDS.


## Technologies
Application:

- Static HTML, JavaScript and CSS  to reduce connections with API and APP. One suggestion is, if possible to have this kind of frontend, is host it on AWS S3. It improves the performance and reliability of static content for web browsers, mainly.

- Python with Flask for REST API and Backend applications
   - The Flask allows use python code to run as Web Server , useful for small tasks in infrastructure, jobs, i.e.

- Docker Containers to deploy the services in each environment;

- Kubernetes for Docker orchestration and IaC.
  - Enable manage different kind of environments(in namespaces), like dev, test, homolog, prod, among others, using the same code.
  - It also handle the scalability features among the clusters
  - Federation: Using homologated Kubernetes engines, it is possible to deploy across Kubernetes engines providers using the same codes, some example of engines:
      - For local environments, _Minikube_ and _Microk8s_. For this project the all Kubernetes were deployed on _Microk8s_;
      - For Enterprise environments, _OKD_ and _Openshift_ are great examples for private datacenter scenario;
      - On cloud, each cloud provider has theirs administrated clusters, for example:
          - Amazon AWS uses EKS;
          - Microsoft Azure uses AKS;
          - Google Cloud uses GKE;
          - Digital Ocean uses DOCKS;
     
- Amazon AWS used as Cloud Provider:
     - Using Firewalls and managing access;
     - MYSQL via AWS RDS as relational database managed by AWS;
     - AWS SQS as queuing service.



- Ansible playbooks to automate and some operations and deploys.

## Steppingstone

### Infrastructure, services and tools
#### playbook-create-vpc-and-network.yml
To speedup the AWS infrastructure creation, I used to use the playbook "*npc-iac/ansible_helper/playbook-create-vpc-and-network.yml*". This is a very generic Ansible playbook to start the configuration. For this step the *aws* terminal client should be properly configured at workstation even it hadn't been directly used.

```bash
cd name-color-pet/ncp-iac/ansible_helper
ansible-playbook playbook-create-vpc-and-network.yml
```

#### Manual EC2 creation
Considering the idea is show the solution working over docker and Kubernetes technologies, an instance was created over an Ubuntu 16.04 _(ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-20190212)_ .
In this case the Ubuntu was chosen because it already brings the _Snapcraft_ and this content manager is very helpful to install the _Microk8s_.
In adiction, the _Docker_ and _docker-compose_ can be installed for any test with the containers.


##### Installing tools

* _Docker_ through _APT_ and _docker-compose_ using _python-pip_

```bash

sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get install apt-transport-https  ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io -y --allow-unauthenticated

sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker
sudo chkconfig docker on

```

* _docker-compose_ via python-pip

```bash  
sudo apt install python-pip -y
pip install docker-compose

```

* _Ansible_ through _pip_. And, also, the _openshift_ module to suport _Kubernetes_ modules.

``` bash
pip install ansible openshift --user --upgrade

```


* _Microk8s_ through _Snapcraft_ with a generic configuration


```bash
	
sudo snap install microk8s --classic --channel=1.14/stable

mkdir -p $HOME/.kube
microk8s.kubectl config view --raw > $HOME/.kube/config

sudo snap alias microk8s.kubectl kubectl
microk8s.enable dns dashboard ingress istio

```

#### AWS services

##### Relational database - Amazon RDS with MySQL
Relational databases whose the replicas, security and disaster recovery procedures are managed by AWS services according configuration. To create it using Ansible, the [rds-module](https://docs.ansible.com/ansible/latest/modules/rds_module.html#rds-module), but it will be necessary to connect in database created to configure the database, table and user access. Highlighting, the administrator never should be set to access the database in any applications. To create it, execute the sql commands inside _ncp-iac/sql/npcdb.sql_ file.

##### Queuing service - Amazon SQS
The AWS SQS is a queuing focused not only in performance but also in realibility considering, for example, the "dead-letter-queue" feature.
Even fast to configure on Web Console, it can be created also using, with Ansible, the [sqs-queue-module](https://docs.ansible.com/ansible/latest/modules/sqs_queue_module.html#sqs-queue-module).

##### Load Balancer (ALB/ELB) with SSL + Domain Name Service (Route53)
To improve the user experience and also create another security layer for customer when Web App or, even, Rest API is requested. The AWS Load Balancers brings, besides the balancing feature, the "CloudWatch" to monitoring the connections every time and the possibility to add a valid SSL Certificate (generated in AWS KMS module) associated with the domain name. This approach besides brings more security into the communication between the end user and application services, increase the user experience due the own Web Browser notices it is an encrypted connection with a trusted (green) SSL certificate.

##### AWS S3
Static web application (HTML,JavaScript + CSS) stored on S3. Working as static web server, it share the reliability and replication among AWS structure without extra configuration. Due its distribution, the download from web browser is faster than if we had it configured inside containers infrastructure.


### Project
#### Download project on instance
To get the project files, use a "git clone" for _muriloaj/name-color-pet_ repository on bitbucket. 

```
git clone https://muriloaj@bitbucket.org/muriloaj/name-color-pet.git
```

#### Using docker-compose
If you want to check how this project should works, there is an example in _docker-compose_ at _name-color-pet/ncp-iac/docker-compose/*docker-compose.yaml*_. Open it and update the environment variables properly with the same values will be used on _configmaps_.
Note inside project file, the ports *7990* and *7999* are reserved for HTML App and Rest API, respectively.

Before start the _docker-compose_, it is necessary create the docker network, the command to do it is ```docker network create ncp_network```.
To start the project using _docker-compose_, use ```docker-compose up -d``` .
To close the project using _docker-compose_, use ```docker-compose down ``` .

For more information about variables, please, check the session regarding Kubernetes's _configmap_.

To become safer to expose the services content in docker-compose structure, an _HAProxy_ was implemented to receive external requests inside this configuration, merging all services into the port *8888*. 

#### Deploying on Microk8s using Ansible
All content related with the Kubernetes are located inside _name-color-pet/ncp-iac/kubernetes_ directory.
As this project is open for world, the variables in _configmap_ must to be set before execution. To do it, rename the files _npc-backend-configmap.yaml__  and _npc-frontend-configmap.yaml__ to _npc-backend-configmap.yaml_  and _npc-frontend-configmap.yaml_ (remove the underscore besides the YAML extension).

In sequence, apply the Kubernetes files using the playbook *playbook-deploy-npc.yml* in same directory

``` ansible-playbook playbook-deploy-npc.yml ```

> Troubleshooting in case of the pods do not reach the SQS service.([link for reference](https://microk8s.io/docs/))
> > 1: Review ACL and Security Groups rules
> > 
> > 2: Make sure packets to/from the pod network interface can be forwarded to/from the default interface on the host
> > >The MicroK8s inspect command can be used to check the firewall configuration: ``` microk8s.inspect ```
> > >Using updating iptables of host machine: ``` sudo iptables -P FORWARD ACCEPT ```
> > >Or using ufw: ``` sudo ufw default allow routed ```


## Links
### Application and API
To see how these application works together, please use the links bellow:

- For application, Web app responsive: 
    - [https://s3.amazonaws.com/npc-s3-html/index.html](https://s3.amazonaws.com/npc-s3-html/index.html) : for application totally static on S3;
    - [https://npcapp.proaject.co/](https://npcapp.proaject.co/) : Same static HTML but running inside a NginX container.

- API Endpoints: 
    - [https://npcapi.proaject.co/npc-submission](https://npcapi.proaject.co/npc-submission) - (POST)
    - [https://npcapi.proaject.co/pet-allowed](https://npcapi.proaject.co/pet-allowed) - (GET,POST): list of pets allowed by service;
    - [https://npcapi.proaject.co/color-allowed](https://npcapi.proaject.co/color-allowed) - (GET,POST) : list of color allowed by service;

### Reference of the technologies related with this project

- [Snapcraft](https://snapcraft.io)
- [Python using Flask framework](http://flask.pocoo.org/)
- [Postman](https://www.getpostman.com/)
- [NginX](https://hub.docker.com/_/nginx)
- [Microk8s](https://microk8s.io/)
- [Kubernetes (K8s)](https://kubernetes.io)
- [HAProxy](http://www.haproxy.org/)
- [GoDaddy](https://godaddy.com/domains)
- [Docker](https://docs.docker.com/)
- [Docker-compose](https://docs.docker.com/compose/compose-file/)
- [Docker Hub](https://hub.docker.com/search?q=%22muriloaj%2Fncp-%22&type=image&sort=updated_at&order=desc)
- [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/core/boto3.html)
- [Ansible](https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html)
- [Amazon Virtual Private Cloud](https://aws.amazon.com/vpc/)
- [Amazon Simple Queue Service](https://aws.amazon.com/sqs/)
- [Amazon S3](https://aws.amazon.com/s3/)
- [Amazon Route 53](https://aws.amazon.com/route53/)
- [Amazon RDS for MySQL](https://aws.amazon.com/rds/mysql/)
- [Amazon ELB](https://aws.amazon.com/elasticloadbalancing/)
- [Amazon EC2](https://aws.amazon.com/ec2/)
- [AWS Key Management Service (KMS)](https://aws.amazon.com/kms/) 
- [AWS Command Line Interface](https://aws.amazon.com/cli/)
