-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Apr 02, 2019 at 07:00 PM
-- Server version: 5.6.43
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `npcdb`
--
CREATE DATABASE IF NOT EXISTS `npcdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `npcdb`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_name_color_pet`
--

DROP TABLE IF EXISTS `tbl_name_color_pet`;
CREATE TABLE IF NOT EXISTS `tbl_name_color_pet` (
  `name` varchar(200) NOT NULL,
  `pet` varchar(200) NOT NULL,
  `color` varchar(200) NOT NULL,
  `creation_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`),
  UNIQUE KEY `unique_username` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_name_color_pet`
--
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE USER 'npc_user'@'%' IDENTIFIED BY 'password@';
GRANT DELETE, INSERT, SELECT, UPDATE ON npcdb.* TO 'npc_user'@'%';
FLUSH PRIVILEGES; 